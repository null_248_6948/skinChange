//
//  OtherViewController.swift
//  skinChangeTo
//
//  Created by xmg on 2016/11/21.
//  Copyright © 2016年 hjjuny. All rights reserved.
//

import UIKit

class OtherViewController: UIViewController {

    @IBOutlet weak var bgImageView: UIImageView!
    
    @IBOutlet weak var iconImageBtn: UIButton!
    
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //    #01.按照提供的图片用对应方法设置皮肤
//        bgImageView.image = Tool.imageName("back.png")
//        iconImageBtn.setImage(Tool.imageName("icon.png"), for: .normal)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        bgImageView.image = SkinTools.imageName("back.png")
        iconImageBtn.setImage(SkinTools.imageName("icon.png"), for: .normal)
        label.backgroundColor = SkinTools.labelBgColor()
    }


}


















