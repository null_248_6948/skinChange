//
//  ViewController.swift
//  skinChangeTo
//
//  Created by xmg on 2016/11/20.
//  Copyright © 2016年 hjjuny. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var iconImageBtn: UIButton!
    @IBOutlet weak var skinViewTopCons: NSLayoutConstraint!
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //// MARK:- 系统回调
        setCurrentSkin()
    }
    
}

//// MARK:- 处理业务逻辑
extension ViewController {
    @IBAction func changeBtn(_ sender: UIBarButtonItem) {
        
        skinViewTopCons.constant = skinViewTopCons.constant == 0 ? -44 : 0
        
        UIView.animate(withDuration: 0.35, animations: {
            
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func changeViewClick(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            SkinTools.setCurrentSkinName("guoqing")
            setCurrentSkin()
        case 2:
            SkinTools.setCurrentSkinName("zhongqiu")
            setCurrentSkin()
        case 3:
            SkinTools.setCurrentSkinName("chunjie")
            setCurrentSkin()
        default:
            print("主题不存在")
        }
    }
    
    fileprivate func setCurrentSkin() {
        // 1.切换皮肤图片
        bgImageView.image = SkinTools.imageName("back.png")
        iconImageBtn.setImage(SkinTools.imageName("icon.png"), for: .normal)
        
        // 2.切换背景颜色
        label.backgroundColor = SkinTools.labelBgColor()
    }
    
    
}






















