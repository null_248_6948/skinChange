//
//  Tool.swift
//  skinChangeTo
//
//  Created by xmg on 2016/11/21.
//  Copyright © 2016年 hjjuny. All rights reserved.
//

import UIKit

private let skinNameKey = "skinNameKey"
class SkinTools: NSObject {
    
    static var skinName: String = ""
    
    override class func initialize() {
        
        //    #03.设置默认皮肤(若沙盒中存有皮肤则默认的无效)
        skinName = UserDefaults.standard.object(forKey: skinNameKey) as? String ?? "guoqing"
        
    }
    
}

extension SkinTools {
    
    class func imageName(_ name: String) -> UIImage? {
        
         return UIImage(named: "Skin/\(skinName)/\(name)")
    
    }
    
    class func setCurrentSkinName(_ skinName: String) {
        self.skinName = skinName
                //    #02.把当前皮肤存到沙盒中
        UserDefaults.standard.set(skinName, forKey: skinNameKey)
        UserDefaults.standard.synchronize()
    }
    class func labelBgColor() -> UIColor {
        switch skinName {
        case "chunjie":
            return .red
        case "zhongqiu":
            return .green
        case "guoqing":
            return .blue
        default:
            return .red
        }
    }

    
}

















